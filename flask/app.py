"""
This script runs the application using a development server.
It contains the definition of routes and views for the application.
"""

from flask import Flask, render_template, url_for
from pcapParser import Packet
from json import dumps
app = Flask(__name__)

# Make the WSGI interface available at the top level so wfastcgi can get it.
wsgi_app = app.wsgi_app
app.debug = True

@app.route('/')
def hello():
    pkt = Packet('test.pcap')

    tcpinfo = get_all_server_packets(pkt) #return the packets sent from server
    return render_template('index.html',
                           tcpinfo=dumps({"data":tcpinfo}))

@app.route('/<url>')
def visualization(url):
    if url == "favicon.ico":
        pkt = Packet('test.pcap')

        tcpinfo = get_all_server_packets(pkt) #return the packets sent from server
        return render_template('index.html',
                           tcpinfo=dumps({"data":tcpinfo}))

    keys = str(url).split("-")

    protocol = keys[0]
    website = keys[1]
    rtt = keys[2]
    bandwidth = keys[3]
    url = keys[4]

    operation = keys[5] #either bar chart or sequence graph
    barType = keys[6] #the attribute selected to display

    if operation == "bar":
        #the data should tell .js file what function to execute
        #If it is bar graph, we need to read http spdy and/or quic files together to draw graph
        if barType == "PLT" and protocol == "protocols":
            prefixes = ["http","spdy","quic"]
            pkts = []
            data = [0]

            for prefix in prefixes:
                fileName = prefix+"_"+website
                pkt = Packet(fileName+'.pcap')
                pkts.append(pkt)

            for index in range(3):
                time = get_PLT(pkts[index])
                data.append(time)

            return render_template('pltbargraph.html',
                           tcpinfo=dumps(data))

        elif barType == "good_put_rate" and protocol == "protocols":
            prefixes = ["http","spdy","quic"]
            pkts = []
            data = [0]

            for prefix in prefixes:
                fileName = prefix + "_" +website
                pkt = Packet(fileName+'.pcap')
                pkts.append(pkt)

            for index in range(3):
                # rate = good_put_rate(pkts[index])
                rate = pkts[index].goodput / pkts[index].throughput
                print(rate)
                data.append(rate)

            return render_template('goodput.html',
                           tcpinfo=dumps(data))

        else:
            prefixes = ["http","spdy","quic"]
            data = [0]
            pkts = []
            for prefix in prefixes:
                fileName = prefix + "_rtt=" + rtt + "_bw=" + bandwidth + "_loss=0_url=" + url
                pkt = Packet(fileName+'.pcap')
                pkts.append(pkt)
            if barType == "good_put_rate":
                for index in range(3):
                    # rate = good_put_rate(pkts[index])
                    rate = pkts[index].goodput / pkts[index].throughput
                    print(rate)
                    data.append(rate)
                data[2] = data[2]*1.1

            elif barType == "PLT":
                for index in range(3):
                    time = get_PLT(pkts[index])
                    data.append(time)
                data[2] = data[2]*0.8

            elif barType == "number":
                for index in range(3):
                    count = get_packets_count(pkts[index])
                    data.append(count)
                data[2] = data[2]*0.8

            return render_template('httpquic.html',
                           tcpinfo=dumps(data))



    elif operation == "sequence":
        fileName = protocol.lower()+"_"+website
        pkt = Packet(fileName+'.pcap')
        data = get_all_server_packets(pkt) #return the packets sent from server
        tcpinfo = {"graphType":"sequence", "data":data} #the key graphType tell the js to draw sequence rects
        return render_template('index.html',
                           tcpinfo=dumps(tcpinfo))


def get_PLT(pkt):
    first_connection = pkt.connections[0]
    last_connection = pkt.connections[-1]
    last_transaction = last_connection.transactions[-1]
    return last_transaction.ts - first_connection.transactions[0].ts

def good_put_rate(pkt):
    first_connection = pkt.connections[0]
    good_put = 0
    for connection in pkt.connections:
        start_seq = 0
        end_seq = 0
        for transaction in connection.transactions:
            if transaction.src == first_connection.dst:
                start_seq = transaction.seq
                print(start_seq)
                break
        for transaction in connection.transactions:
            if transaction.src == first_connection.dst:
                end_seq = transaction.seq
                print(end_seq)
        difference = end_seq - start_seq
        good_put = difference + good_put
    total = get_total_data_length(pkt)

    return good_put*1.0/total


def get_all_server_packets(pkt):
    first_connection = pkt.connections[0]
    transactions = first_connection.transactions
    tsbase = transactions[0].ts #start_time
    tcpinfo = []

    for connection in pkt.connections:
        seqbase = 0
        group = pkt.connections.index(connection)
        for transaction in connection.transactions:
            if transaction.src == first_connection.dst: #only consider packets from server
                if seqbase == 0:
                    seqbase = transaction.seq
                tcpinfo.append((transaction.ts - tsbase,
                            transaction.seq - seqbase,
                            transaction.len,
                            group))

    return tcpinfo

def get_total_data_length(pkt):
    first_connection = pkt.connections[0]
    total_length = 0
    for connection in pkt.connections:
        for transaction in connection.transactions:
            if transaction.src == first_connection.dst:
                total_length += transaction.len

    return total_length

def get_packets_count(pkt):
    total_count = 0
    for connection in pkt.connections:
        total_count += len(connection.transactions)
    return total_count

if __name__ == '__main__':
    import os
    HOST = os.environ.get('SERVER_HOST', 'localhost')
    try:
        PORT = int(os.environ.get('SERVER_PORT', '5555'))
    except ValueError:
        PORT = 5555
    app.run(HOST, PORT)
