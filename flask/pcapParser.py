import dpkt
import socket
import codecs

class Transaction(object):
    def __init__(self, ts, data, src, dst):
        self.ts = ts
        self.src = src
        self.dst = dst
        if isinstance(data, dpkt.tcp.TCP):
            self.transProtocol = 'TCP'
            self.seq = data.seq
            self.len = len(data.data)
            try:
                if data.dport == 80 and self.len > 0:
                    self.http = dpkt.http.Request(data.data.decode('utf-8'))
                    #print(self.http.method)
                    #print(self.http.uri)
                elif data.sport == 80 and self.len > 0:
                    self.http = dpkt.http.Response(data.data.decode('utf-8'))
            except Exception as e:
                self.http = "unknown"

        elif isinstance(data, dpkt.udp.UDP):
            self.transProtocol = 'UDP'
            # print(data.sport)
            # print(data.dport)
            CIDlen = 8 if (data.data[0] & 0x0c) >> 2 == 3 else 0
            self.cid = int.from_bytes(data.data[1:1+CIDlen], byteorder='little')
            hasVersion = (data.data[0] & 0x01) == 1
            self.seq = data.data[1+CIDlen+ (4 if hasVersion else 0)]
            self.len = len(data.data)


class Connection(object):
    def __init__(self, src, dst):
        self.src = src
        self.dst = dst
        self.transactions = []

    def match(self, src, dst):
        if self.src == src and self.dst == dst:
            return True
        else:
            return (self.src == dst and self.dst == src)

    def insert(self, ts, data, src, dst):
        tr = Transaction(ts, data, src, dst)
        self.transactions.append(tr)
        return tr.len

class Packet(object):
    def __init__(self, filename):
        assert isinstance(filename, str)
        self.filename = filename
        f = open(filename, 'rb')
        #print filename
        pcap = dpkt.pcap.Reader(f)

        self.connections = []
        self.throughput = 0
        self.goodput = 0
        for ts, buf in pcap:
            self.throughput += len(buf)
            eth = dpkt.ethernet.Ethernet(buf)
            ip = eth.data
            self.goodput += self.__insertTransaction(ts, ip)

        f.close()

    def __insertTransaction(self, ts, ip):
        assert isinstance(ip, dpkt.ip.IP)
        if isinstance(ip.data, dpkt.tcp.TCP) or isinstance(ip.data, dpkt.udp.UDP):
            data = ip.data
            src = socket.inet_ntoa(ip.src) + ':' + str(data.sport)
            dst = socket.inet_ntoa(ip.dst) + ':' + str(data.dport)
            for connection in self.connections:
                if connection.match(src, dst):
                    return connection.insert(ts, data, src, dst)
            self.connections.append(Connection(src, dst))
            return self.connections[-1].insert(ts, data, src, dst)
        else:
            raise ValueError("ip.data is neither TCP nor UDP.")

if __name__ == "__main__":
    try:
        # pkt = Packet('spdy_google.pcap')
        # print("# connections: ", len(pkt.connections))
        pkt = Packet('quic_google.pcap')
        print("# connections: ", len(pkt.connections))
    except Exception as e:
        print(e)