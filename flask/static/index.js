/**
 * Created by lkchen on 4/3/2016.
 */

function myFunction(){
    var protocol = document.getElementById("Protocols").value;
    var website = document.getElementById("Website").value;
    var rtt = document.getElementById("RTT").value;
    var bandwidth = document.getElementById("Bandwidth").value;
    var url = document.getElementById("url").value;
    var graph = document.getElementById("Graph").value;
    var bar_type = document.getElementById("BarType").value;

    var prefix = "http://localhost:5555/";
    var suffix = protocol + "-" + website + "-" + rtt + "-" + bandwidth + "-" + url + "-" + graph + "-" + bar_type;
    var link = prefix + suffix;

    window.location.href = link;
}

var svg = d3.selectAll('svg');

var barwidths = [];
var bartimes = [];
for (var i = 0; i < data.length; i++) {
    barwidths.push(data[i][1] + data[i][2]);
    bartimes.push(data[i][0]);
}
var maxWidth = d3.max(barwidths);
var maxTime = d3.max(bartimes);
var svgWidth = 800;
var svgHeight = 400;
var rectHeight = 3;
var leftPadding = 40;
var botPadding = 10;

var timeScale = d3.scale
    .linear()
    .domain([0, maxTime])
    .range([0, svgHeight]);

var lenScale = d3.scale
    .linear()
    .domain([0, maxWidth])
    .range([0, svgWidth]);

var color = d3.scale.category10();

svg.attr('width', svgWidth + 100)
    .attr('height', svgHeight + 30);

function addxAxies(){
    var axis = d3.svg.axis()
     .scale(lenScale)
     .orient("bottom")
     .ticks(8);


    var y = svgHeight;
    var str = "translate(" + leftPadding.toString() + "," + y.toString()+ ")"
     svg.append("g")
     .attr("transform",str)
     .call(axis);
}

function addyAxies(){
    var timeScaleAxies = d3.scale
    .linear()
    .domain([maxTime, 0])
    .range([0, svgHeight]);

    var axis = d3.svg.axis()
     .scale(timeScaleAxies)
     .orient("left")
     .ticks(10);


    var str = "translate(" + leftPadding.toString() + ",0)"
     svg.append("g")
     .attr("transform",str)
     .call(axis);
}

function drawSecRects(){
    svg.selectAll('rect')
    .data(data)
    .enter()
    .append('rect')
    .attr('x', function(d) {
        return lenScale(d[1]) + leftPadding;
    })
    .attr('y', function(d,i) {
        //return svgHeight-timeScale(d[0]) + data.length*rectHeight;
        return svgHeight-timeScale(d[0])+botPadding;
    })
    .attr('height', rectHeight)
    .attr('width', function(d){
        return lenScale(d[2]);
    })
    .attr("fill", function(d){
      	return color(d[3])
     });

    addxAxies();
    addyAxies();
}

drawSecRects();




