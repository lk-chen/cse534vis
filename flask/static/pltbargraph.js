function myFunction(){
    var protocol = document.getElementById("Protocols").value;
    var website = document.getElementById("Website").value;
    var rtt = document.getElementById("RTT").value;
    var bandwidth = document.getElementById("Bandwidth").value;
    var url = document.getElementById("url").value;
    var graph = document.getElementById("Graph").value;
    var bar_type = document.getElementById("BarType").value;

    var prefix = "http://localhost:5555/";
    var suffix = protocol + "-" + website + "-" + rtt + "-" + bandwidth + "-" + url + "-" + graph + "-" + bar_type;
    var link = prefix + suffix;

    window.location.href = link;
}

var svg = d3.selectAll('svg');
        var svgWidth = 800;
		var svgHeight = 400;
		svg.attr('width', svgWidth)
    	.attr('height', svgHeight);
    	//var data = [0,2,3,4];
    	var texts = ["HTTP", "SPDY", "QUIC"];

    	var max = d3.max(data);
    	var linear = d3.scale.linear()
        				.domain([0, max])
        				.range([0, 350]);

        var linear1 = d3.scale.linear()
        				.domain([max, 0])
        				.range([0, 350]);

    	svg.selectAll("text")
			.data(texts)
			.enter()
			.append("text")
			.attr("x", function(d,i){
				return (i+1)*100;
			})
            .attr("y", function(d,i){
				return svgHeight - linear(data[i+1])-5;
			})
            .attr('fill', 'black')
            .text(function(d){
            	return d;
            });


        var yaxis = d3.svg.axis()
    			.scale(linear1)
    			.orient("left")
    			.ticks(7);
    	svg.append("g")
    		.attr("transform","translate(30,45)")
   			.call(yaxis);

    	svg.selectAll("rect")
    		.data(data)
    		.enter()
    		.append("rect")
    		.attr("x",function(d,i){
        		return i * 100;
    		})
    		.attr("y",function(d,i){
         		return svgHeight-linear(d);
    		})
    		.attr("width",40)
   			.attr("height",function(d,i){
        		return linear(d);
    		})
    		.attr("fill","steelblue");